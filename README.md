# Top Down Shooter

This game is a top down shooter playable with a game controller. The purpose of this project is to try a new style of game that I never developed.

## Getting Started

To run this project you can download this repository then open it in Unity and build it for the platform of your choicece (PC or Android tested).

### Prerequisites

You do not need anything else to run this game.

### Installing

First you need to install Unity 2019.1 or higher recommended. Then build the game (see "Getting Started" section).

If you built the game for PC, you do not need anything else than just running the generated .exe file.
For Android platform you can either build directly to your device from the Unity build window or you can copy the generated .apk file to your device.
Then browse to its location on your device and run it. This will install the game on your device (you might have to activate the developer mode on your device to do that).


## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Jean Bourquard** - *Whole project* - [meli-melo](https://gitlab.com/meli-melo)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details