﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public Transform _player;
    [SerializeField]
    private float _speed = 10f;
    [SerializeField]
    private Rigidbody _rigidbody;
    [SerializeField]
    private int _life = 10;

    [SerializeField]
    private Animator _animator;

    // Start is called before the first frame update
    void Start()
    {
        _speed = Random.Range(8f, 12f);
        //_animator.Play()
    }

    void FixedUpdate()
    {
        _rigidbody.AddForce(transform.forward * _speed);
    }

    // Update is called once per frame
    void Update()
    {
        FacePlayer();
    }

    private void FacePlayer()
    {        
        Quaternion rot = Quaternion.LookRotation(_player.position - transform.position, Vector3.up);
        transform.rotation = rot;
        transform.eulerAngles = new Vector3 (0, transform.eulerAngles.y, 0);
    }

    public void Hit(int hitDmg)
    {
        _life -= hitDmg;
        if(_life <= 0)
            Dead();
    }

    private void Dead()
    {
        Destroy(gameObject);
    }
}
