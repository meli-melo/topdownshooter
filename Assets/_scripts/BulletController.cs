﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    [SerializeField]
    private float _speed;

    [SerializeField]
    private Rigidbody _rigidbody;

    [SerializeField]
    private float _lifeTimer;
    private float _life = 0f;
    [SerializeField]
    private int _damage = 2;

    void Update()
    {
        _life += Time.deltaTime;
        if(_life >= _lifeTimer)
            Destroy(gameObject);
    }

    public void SetSpeed(float speed)
    {
        _speed = speed;
        _rigidbody.velocity = transform.forward * _speed;
    }
    
    void OnCollisionEnter(Collision collision)
    {
        EnemyController enemy = collision.gameObject.GetComponent<EnemyController>();
        if(enemy != null)
            enemy.Hit(_damage);
    }
}
