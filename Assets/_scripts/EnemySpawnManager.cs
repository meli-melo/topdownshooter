﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnManager : MonoBehaviour
{
    [SerializeField]
    private List<Transform> _spawners;

    [SerializeField]
    private GameObject _enemyPrefab;

    [SerializeField]
    private float _spawningSpeed = 1f;

    [SerializeField]
    private Transform _player;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnEnemy", 0f, _spawningSpeed);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void SpawnEnemy()
    {
        int i = Random.Range(0, _spawners.Count);
        GameObject enemy = Instantiate(_enemyPrefab, _spawners[i].position, Quaternion.identity);
        enemy.GetComponent<EnemyController>()._player = _player;
    }
}
