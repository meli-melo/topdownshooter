﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private GameObject _player;
    private Vector3 _offset;

    // Start is called before the first frame update
    void Start()
    {
        _offset = _player.transform.position - transform.position;
    }

    void FixedUpdate()
    {
        transform.position = new Vector3(_player.transform.position.x - _offset.x, transform.position.y, _player.transform.position.z - _offset.z);
    }
}
