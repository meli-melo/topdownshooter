﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region movement
    [SerializeField]
    private Rigidbody _rigidbody;

    [SerializeField]
    private float _horizontal = 0f;
    [SerializeField]
    private float _vertical = 0f;
    [SerializeField]
    private float _speed = 1f;

    private Vector3 _mousePosition;
    private Vector3 _lootAtTargetPosition;
    #endregion

    #region shoot
    [SerializeField]
    private Transform _bulletSpawnPosition;
    [SerializeField]
    private GameObject _bulletPrefab;
    [SerializeField]
    private float _bulletSpeed = 10f;
    [SerializeField]
    private bool _autoFire = false;
    #endregion

    private Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    void FixedUpdate()
    {
        _horizontal = Input.GetAxis("Horizontal");
        _vertical = Input.GetAxis("Vertical");

        if(Mathf.Abs(_horizontal) > 0 || Mathf.Abs(_vertical) > 0)
        {
            _rigidbody.AddForce(new Vector3(_horizontal * _speed, 0f, _vertical * _speed));
        }   
    }

    // Update is called once per frame
    void Update()
    {
        RotatePlayer();

        if(_autoFire)
        {
            if(Input.GetButton("Fire1"))
                ShootBullet();
        }
        else
        {
            if(Input.GetButtonDown("Fire1"))
                ShootBullet();
        }

    }

    private void RotatePlayer()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            _lootAtTargetPosition = hit.point;
        }
        
        if(_lootAtTargetPosition != Vector3.zero)
        {
            Quaternion rot = Quaternion.LookRotation(transform.position + _lootAtTargetPosition, Vector3.up);
            transform.rotation = rot;
            transform.eulerAngles = new Vector3 (0, transform.eulerAngles.y, 0);
        }
    }

    private void ShootBullet()
    {
        GameObject bullet = Instantiate(_bulletPrefab, _bulletSpawnPosition.position, transform.rotation);
        bullet.GetComponent<BulletController>().SetSpeed(_bulletSpeed);
    }
}
